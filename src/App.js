import React from 'react';
import {Root, Button, Icon} from 'native-base';
import {StatusBar, View, Image} from 'react-native';
import {
  createStackNavigator,
  createAppContainer,
  createSwitchNavigator,
  createBottomTabNavigator,
} from 'react-navigation';
console.disableYellowBox = true;

import Bhajanai from './screens/anatomy/home/Bhajanai';
import Welcome from './screens/anatomy/Onboard/WelcomeScreen';
import Album from './screens/album/index.js';
import GodAlbum from './screens/album/GodAlbum';
import Player from './screens/player/index.js';

import Photos from './screens/anatomy/Photo/Photos';
import PhotosDetailsView from './screens/anatomy/Photo/PhotosDetailsView';
import About from './screens/anatomy/About/About';
//import MyWeb from './screens/anatomy/Webview/MyWeb';
import Songs from './screens/anatomy/Songs/Songs';

// Tab screen

import EbookScreen from './screens/anatomy/ebook/EbookScreen';
import MoreScreen from './screens/anatomy/More/MoreScreen';

import Tabscreen from './screens/anatomy/Songs/tabs/Tabscreen';

import ArtistPlay from './screens/anatomy/Songs/tabs/ArtistPlay';
import Artistlist from './screens/anatomy/Songs/tabs/Artistlist';
import Godnamelist from './screens/anatomy/Songs/tabs/Godnamelist';
import GodDetailList from './screens/anatomy/Songs/tabs/GodDetailList';

import ArtistDetailList from './screens/anatomy/Songs/tabs/ArtistDetailList';

import Ereader from './screens/anatomy/ebook/Ereader';

const DashboardTabNavigator = createBottomTabNavigator(
  {
    Bhajanai: {
      screen: Bhajanai,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Image
            source={require('../assets/icons/Tab/temple.png')}
            style={{height: 24, width: 24, tintColor: tintColor}}
          />
        ),
      },
    },
    Ebook: {
      screen: EbookScreen,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Image
            source={require('../assets/icons/Tab/newspaper.png')}
            style={{height: 32, width: 32, tintColor: tintColor}}
          />
        ),
      },
    },
    More: {
      screen: MoreScreen,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Image
            source={require('../assets/icons/Tab/more.png')}
            style={{height: 24, width: 24, tintColor: tintColor}}
          />
        ),
      },
    },
  },
  {
    navigationOptions: ({navigation}) => {
      const {routeName} = navigation.state.routes[navigation.state.index];
      return {
        headerTitle: routeName,
        headerTitleStyle: {
          textAlign: 'center',
          flex: 1,
        },
      };
    },
  },
);

const AppNavigator = createStackNavigator(
  {
    DashboardTabNavigator: {
      screen: DashboardTabNavigator,
    },
    Tabscreen: Tabscreen,
    ArtistPlay: ArtistPlay,
    Artistlist: {
      screen: Artistlist,
      navigationOptions: {
        title: 'Artist Albums',
      },
    },
    Godnamelist: {
      screen: Godnamelist,
      navigationOptions: {
        title: 'God Albums',
      },
    },
    GodDetailList: GodDetailList,
    ArtistDetailList: ArtistDetailList,
    Ereader: Ereader,
    Album: Album,
    GodAlbum: GodAlbum,
    Player: Player,
    Bhajanai: Bhajanai,
    Songs: Songs,
    Photos: {
      screen: Photos,
      navigationOptions: {
        title: 'Photos',
      },
    },
    PhotosDetailsView: PhotosDetailsView,
    About: {
      screen: About,
      navigationOptions: {
        title: 'About',
      },
    },
    // MyWeb: {
    //   screen: MyWeb,
    //   navigationOptions: {
    //     title: 'Ebook Details',
    //   },
    // },
  },
  {
    defaultNavigationOptions: () => {
      return {
        headerBackImage: (
          <Image
            source={require('./../assets/icons/nav-icons/left-arrow.png')}
            style={{height: 24, width: 24, marginLeft: 18}}
          />
        ),
        headerBackTitle: null,
        headerTitleStyle: {
          textAlign: 'center',
          flex: 1,
          marginRight: 80,
        },
        headerStyle: {
          elevation: 0,
        },
      };
    },
  },
  {
    navigationOptions: ({navigation}) => {
      const {routeName} = navigation.state.routes[navigation.state.index];
      return {
        headerTitle: routeName,
        headerMode: 'screen',
      };
    },
  },
);

const AppSwitchNavigator = createSwitchNavigator({
  Welcome: {screen: Welcome},
  Dashboard: {screen: AppNavigator},
  Tabscreen: Tabscreen,
});

const WrappedDrawer = createAppContainer(AppSwitchNavigator);

export default () => (
  <Root>
    <WrappedDrawer />
  </Root>
);
