const React = require('react-native');
const { Dimensions } = React;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    backgroundColor: '#f7f7f7',
    flex: 1,
    zIndex: 50,
  },
  center: {
    flex: 1,
    alignItems: 'center',
  },
  trackView: {
    paddingTop: 24,
    flexDirection: 'row',
    paddingLeft: 20,
    alignItems: 'center',
    paddingRight: 20,
    marginTop: 320,
  },
  detailsWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
  artist: {
    color: '#f44336',
    fontSize: 12,
    marginTop: 4,
  },
  slider: {
    flex: 1,
    alignItems: 'center',
    marginBottom: 30,
  },
  seekView: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 16,
  },
  seekBar: {
    width: deviceWidth - 20,
    marginTop: 25,
  },
  controls: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 50,
    marginLeft: 50,
  },
  play: {
    height: 55,
    width: 55,
    borderWidth: 1,
    borderColor: '#383838',
    borderRadius: 55 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  time: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: -20,
  },
  timeChildView: {
    alignItems: 'center',
    flex: 0.15,
  },
  timeChildViewText: {
    color: '#383838',
    fontSize: 12,
  },
  iconWhite: {
    color: '#383838',
  },
  btnSection: {
    backgroundColor: 'transparent',
    alignItems: 'center',
    height: 40,
    alignSelf: 'center',
  },
  btnTransparent: {
    backgroundColor: 'transparent',
  },
  width10: {
    width: 10,
  },
  width30: {
    width: 30,
  },
};
