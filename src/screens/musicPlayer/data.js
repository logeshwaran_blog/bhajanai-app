export default musicPlayerData = {
  source:
    'https://assets-music.icons8.com/free-download/86/033d4211-a5e6-4006-835b-814c30239126.mp3?filename=rainbows.mp3',
  tunes: [
    {
      name:
        'அறுபடை வீடு கொண்ட திருமுருகா திருமுருகாற்றுப்படை தனிலே வருமுருகா -முருகா',
      artists: 'Rudimental, Jess Glyme, Macklemore',
      img: require('./../../../assets/Rectangle.jpg'),
      url:
        'http://yb8.3a6.myftpupload.com/wp-content/uploads/2019/12/எந்த-மலை-சேவித்தாலும்.mp3',
    },
  ],
  playlistName: 'Summer Splash',
  currentTrackIndex: 0,
  minimal: false,
};
