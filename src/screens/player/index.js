/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Container,
  Content,
  Button,
  Icon,
  Header,
  Left,
  Body,
  Right,
  Title,
} from 'native-base';
import {Image, View, Text, Modal} from 'react-native';
import Swiper from 'react-native-deck-swiper';
import MusicPlayer from '../musicPlayer/index';
import styles from './styles';
import ModalList from '../../component/ModalList';

import {NavigationFocusInjectedProps, withNavigation} from 'react-navigation';

console.disableYellowBox = true;

let times = 0;

class Player extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    const {params} = this.props.navigation.state;

    console.log('this.params');
    console.log(params.dataSource);
    console.log('params.currentTrackIndex');
    console.log(params.currentTrackIndex);

    let newArr = [];
    for (
      var i = parseInt(params.currentTrackIndex, 10);
      i < params.dataSource.length;
      i++
    ) {
      newArr.push(params.dataSource[i]);
    }
    for (var i = 0; i < parseInt(params.currentTrackIndex, 10); i++) {
      newArr.push(params.dataSource[i]);
    }
    console.log('newArr');
    console.log(newArr);

    this.state = {
      tunes: params.dataSource,
      currentTrackIndex: params.currentTrackIndex,
      queueModal: false,
    };
    console.log('state');
    console.log(this.state);
    // console.log('this.state.tunes.url');
    // console.log(this.state.tunes.url);

    console.log('URL');

    // console.log(this.state.tunes[0].url);

    // console.log(this.props.navigation.state.params);
    // console.log(this.props.navigation.state);
    // console.log(this.props.navigation);
    this.upCurrentTrackIndex = this.upCurrentTrackIndex.bind(this);
  }

  upCurrentTrackIndex = () => {
    let currentTrackIndex = this.state.currentTrackIndex + 1;
    if (this.state.currentTrackIndex === this.state.tunes.length - 1) {
      currentTrackIndex = 0;
    }

    this.setState({
      currentTrackIndex: currentTrackIndex,
    });
    this.forceUpdate();
  };

  downCurrentTrackIndex = () => {
    let currentTrackIndex = this.state.currentTrackIndex - 1;
    if (this.state.currentTrackIndex === 0) {
      currentTrackIndex = this.state.tunes.length - 1;
    }

    this.setState({
      currentTrackIndex: currentTrackIndex,
    });
    this.forceUpdate();
  };

  leftSwipe = () => {
    this._swiper.swipeLeft(true);
    console.log('this._swiper');
    console.log(this._swiper);

    this.forceUpdate();
  };

  rightSwipe = () => {
    this._swiper.swipeRight(true);
    this.forceUpdate();
  };

  setQueueModal = () => {
    this.setState({
      queueModal: true,
    });
  };
  closeQueueModal = () => {
    this.setState({
      queueModal: !this.state.queueModal,
    });
  };

  render() {
    console.log('render');
    console.log(this.state.tunes);
    console.log(this.state.url);
    console.log('this.state.currentTrackIndex');
    console.log(this.state.currentTrackIndex);

    times += 1;
    return (
      <Container style={styles.container}>
        <Header
          style={styles.header}
          androidStatusBarColor="#222325"
          iosBarStyle="light-content">
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-down" style={styles.colorWhite} />
            </Button>
          </Left>
          <Body>
            <Title style={styles.headerTitle} />
          </Body>
          <Right />
        </Header>

        <Content>
          <View>
            <View style={styles.bottomMargin}>
              <Swiper
                cards={this.state.tunes}
                ref={swiper => {
                  this._swiper = swiper;
                }}
                cardIndex={this.state.currentTrackIndex}
                backgroundColor={'white'}
                showSecondCard={false}
                goBackToPreviousCardOnSwipeLeft={true}
                infinite={true}
                cardVerticalMargin={20}
                marginBottom={40}
                cardStyle={styles.swiperStyle}
                onSwipedLeft={this.upCurrentTrackIndex}
                onSwipedRight={this.downCurrentTrackIndex}
                renderCard={tune => {
                  console.log('tune');
                  console.log(tune);
                  return (
                    <View style={styles.center}>
                      <Image
                        style={styles.albumArt}
                        source={{uri: tune.cover_full}}
                        backgroundColor="#f7f7f7"
                      />

                      <View
                        style={{
                          alignItems: 'center',
                          flexDirection: 'row',
                          paddingTop: 20,
                        }}>
                        <View style={styles.detailsWrapper}>
                          <Text style={styles.title}>{tune.title}</Text>
                          <Text style={styles.artist}>{tune.artists}</Text>
                        </View>
                      </View>
                    </View>
                  );
                }}
              />
            </View>
            <MusicPlayer
              source={this.state.tunes[0].url}
              tunes={this.state.tunes}
              // playlistName={this.props.navigation.state.params.playlistName}
              currentTrackIndex={this.state.currentTrackIndex}
              swipeLeft={this.leftSwipe}
              swipeRight={this.rightSwipe}
              playInBackground={false}
            />
          </View>
        </Content>
      </Container>
    );
  }
}

export default Player;
