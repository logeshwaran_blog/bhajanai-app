const React = require('react-native');
const {Dimensions} = React;
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  container: {
    backgroundColor: '#f7f7f7',
    flex: 1,
  },
  header: {
    backgroundColor: 'white',
  },
  headerTitle: {
    width: deviceWidth / 1.5,
    color: 'black',
    fontSize: 14,
  },
  colorWhite: {
    color: 'black',
  },
  swiperStyle: {
    height: deviceHeight / 1.8,
    elevation: 200,
  },
  albumArt: {
    width: deviceWidth - 160,
    height: deviceWidth - 160,
    borderRadius: 12,
  },
  center: {
    flex: 1,
    alignItems: 'center',
  },
  trackView: {
    paddingTop: 24,
    flexDirection: 'row',
    paddingLeft: 20,
    alignItems: 'center',
    paddingRight: 20,
  },
  detailsWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'center',
  },
  artist: {
    color: 'grey',
    fontSize: 12,
    marginTop: 4,
  },
  seekView: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 16,
  },
  seekBar: {
    width: deviceWidth - 20,
    marginTop: 25,
  },
  controls: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    marginRight: 50,
    marginLeft: 50,
  },
  play: {
    height: 55,
    width: 55,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 55 / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  time: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: -20,
    marginTop: deviceHeight / 1.7,
  },
  bottomMargin: {
    height: deviceHeight / 1.6,
  },
};
