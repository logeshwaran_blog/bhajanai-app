/**
 * Audrix (https://www.enappd.com/audrix)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import React, { Component } from "react";
import {
  Container,
  Content,
  Text,
  Body,
  Left,
  Thumbnail,
  List,
  ListItem,
  Header,
  Right,
  Title,
  Button,
  Icon,
  ActionSheet,
  Footer,
  FooterTab
} from "native-base";
import { View, Image, FlatList, TouchableOpacity } from "react-native";
import styles from "./styles";
import MusicPlayerWrapper from "../musicPlayerWrapper/index";
import TouchableNativeFeed from "./../../component/TouchableNativeFeedback";
import offlineContentData from "./data.js";
import Data from "../../constants/constantData.js";

const player = Data.player;
const footerPlayer = Data.footerPlayer;
const songs = offlineContentData.data;
const SONG_BUTTONS = offlineContentData.SONG_BUTTONS;
const SONG_DESTRUCTIVE_INDEX = offlineContentData.SONG_DESTRUCTIVE_INDEX;
const SONG_CANCEL_INDEX = offlineContentData.SONG_CANCEL_INDEX;
class offlineContent extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <Header style={styles.header} androidStatusBarColor="#222325" iosBarStyle="light-content">
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="menu" style={styles.colorWhite} />
            </Button>
          </Left>
          <Body>
            <Title style={styles.headerTitle}>Offline Content</Title>
          </Body>
          <Right />
        </Header>
        <Content style={styles.container}>
          <FlatList data={songs}
            keyExtractor={song => song.name}
            renderItem={(song) =>
              <ListItem
                thumbnail
                noBorder
                onPress={() => this.props.navigation.navigate(
                  {
                    key: player.key,
                    routeName: player.routeName,
                    params: {
                      tunes: songs,
                      currentTrackIndex: song.index,
                      playlistName: player.playlistName
                    }
                  }
                )}
              >
                <Left>
                  <Thumbnail square size={55} source={song.item.img} />
                </Left>
                <Body>
                  <Text style={styles.listText}>
                    {song.item.name}
                  </Text>
                  <Text uppercase numberOfLines={1} note>
                    {song.item.artists}
                  </Text>
                </Body>
                <Right>
                  <Button
                    transparent
                    onPress={() =>
                      ActionSheet.show(
                        {
                          options: SONG_BUTTONS,
                          cancelButtonIndex: SONG_CANCEL_INDEX,
                          destructiveButtonIndex: SONG_DESTRUCTIVE_INDEX,
                          title: "Playlist Options"
                        },
                        buttonIndex => {
                          this.setState({ clicked: SONG_BUTTONS[buttonIndex] });
                        }
                      )
                    }
                  >
                    <Icon name="md-more" style={styles.colorWhite} />
                  </Button>
                </Right>
              </ListItem>}
          />
        </Content>

        <TouchableOpacity
          onPress={() => this.props.navigation.navigate(
            {
              key: player.key,
              routeName: player.routeName,
              params: {
                tunes: [{
                  name: player.params.tunes[0].name,
                  artists: player.params.tunes[0].artists,
                  img: player.params.tunes[0].img,
                  url: player.params.tunes[0].url
                }],
                currentTrackIndex: player.params.currentTrackIndex,
                playlistName: player.params.playlistName
              }
            }
          )}
        >
          <Footer style={styles.footer}>
            <MusicPlayerWrapper minimal={true} />
          </Footer>
        </TouchableOpacity>

      </Container>
    );
  }
}

export default offlineContent;
