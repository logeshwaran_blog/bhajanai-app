/**
 * Audrix (https://www.enappd.com/audrix)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

const React = require("react-native");
const { Dimensions } = React;
const deviceWidth = Dimensions.get("window").width;

export default {
  container: {
    backgroundColor: "black"
  },
  header: {
    backgroundColor: "#222325"
  },
  headerTitle: {
    width: deviceWidth / 1.5,
    color: "white"
  },
  listText: {
    color: "white",
    fontWeight: "bold"
  },
  colorWhite: {
    color: "white"
  },
  footer: {
    backgroundColor: "#222325",
    paddingTop: 1
  }

};
