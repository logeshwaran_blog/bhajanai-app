/**
 * Audrix (https://www.enappd.com/audrix)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import React from "react";
import {
  Button,
  Form,
  Item,
  Label,
  Input,
  Text,
} from "native-base";
import { View } from "react-native";

export default ()=>
        <View>
            <Form>
                <Item floatingLabel>
                <Label style={{paddingTop: 4}}>OTP</Label>
                <Input keyboardType="numeric" maxLength={6} autoFocus={true} />
                </Item>
            </Form>
            <Button block style={{ margin: 15, marginTop: 50, backgroundColor: "black" }}>
                <Text>Verify</Text>
            </Button>
        </View>
;

