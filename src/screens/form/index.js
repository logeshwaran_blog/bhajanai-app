/**
 * Audrix (https://www.enappd.com/audrix)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Form,
  Item,
  Label,
  Input,
  Icon,
  Text,
  Left,
  Body,
  Right,
} from "native-base";
import { Modal, View, Image } from "react-native";
import ForgotPassword from "./forgotPassword.js";
import styles from "./styles";


class Login extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      forgotPasswordModal: false
    };
  }

  setForgotPasswordModal = () => {
    this.setState({
      forgotPasswordModal: true
    });
  }
  closeForgotPasswordModal = () => {
    this.setState({
      forgotPasswordModal: !this.state.forgotPasswordModal
    });
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header style={{ backgroundColor: "#222325" }} androidStatusBarColor="#222325" iosBarStyle="light-content">
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon name="md-arrow-round-back" style={styles.white} />
            </Button>
          </Left>
          <Body>
            <Title style={styles.white}>Login</Title>
          </Body>
          <Right />
        </Header>

        <Content contentContainerStyle={styles.content}>
          <Image source={require("./../../../assets/screen.png")} style={styles.logo} />
          <Form style={styles.form}>
            <Item floatingLabel>
              <Label style={{ paddingTop: 4, textAlign: "center" }}>Username or Email</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label style={{ paddingTop: 4, textAlign: "center" }}>Password</Label>
              <Input secureTextEntry />
            </Item>
          </Form>
          <Button
            block
            style={{ margin: 15, marginTop: 50, backgroundColor: "black" }}
            onPress={() => this.props.navigation.navigate(
              {
                key: "TabHome",
                routeName: "TabHome"
              }
            )}
          >
            <Text>Sign In</Text>
          </Button>

          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.forgotPasswordModal}
            onRequestClose={() => {
              this.props.navigation.goBack();
            }}>
            <Container style={styles.container}>
              <Header style={{ backgroundColor: "#222325" }} androidStatusBarColor="#222325" iosBarStyle="light-content">
                <Left>
                  <Button transparent onPress={this.closeForgotPasswordModal}>
                    <Icon name="md-close" style={styles.white} />
                  </Button>
                </Left>
                <Body style={styles.headerTitle}>
                  <Title style={styles.headerTitle}>Forgot Password</Title>
                </Body>
                <Right />
              </Header>
              <Content>
                <ForgotPassword />
              </Content>
            </Container>

          </Modal>

          <Text style={{ textAlign: "center", fontWeight: "bold", marginBottom: 10, marginTop: 20 }} onPress={this.setForgotPasswordModal}>
            Forgot Password?
          </Text>
          <Text
            style={{ textAlign: "center", fontWeight: "bold" }}
            onPress={() => this.props.navigation.navigate(
              {
                key: "Register",
                routeName: "Register"
              }
            )}
          >
            Not Regsitered? Register here.
          </Text>
        </Content>
      </Container>
    );
  }
}

export default Login;
