/**
 * Audrix (https://www.enappd.com/audrix)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import { Dimensions } from "react-native";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  container: {
    backgroundColor: "#FFF",
    flex: 1
  },
  content: {
    alignItems: "center",
    justifyContent: "center"
  },
  logo: {
    height: deviceHeight / 3,
    width: deviceHeight / 3
  },
  registerLogo: {
    height: deviceHeight / 4.2,
    width: deviceHeight / 4.2
  },
  form: {
    width: deviceWidth / 2.2,
    marginRight: 10
  },
  formLabel:{
    paddingTop: 4, 
    textAlign: "center"
  },
  registerButton:{
    margin: 15, 
    marginTop: 50, 
    backgroundColor: "black" 
  },
  loginText:{
    textAlign:"center", 
    fontWeight: "bold"
  },
  white:{
    color: 'white'
  },
  headerTitle:{
    color: 'white',
    width: deviceWidth / 1.5
  }
};
