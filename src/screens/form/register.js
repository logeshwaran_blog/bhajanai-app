/**
 * Audrix (https://www.enappd.com/audrix)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Form,
  Item,
  Label,
  Input,
  Icon,
  Text,
  Left,
  Body,
  Right,
} from "native-base";
import styles from "./styles";
import { Image } from "react-native";


class Register extends Component {
  static navigationOptions = {
    header: null
  };

    render() {
      return (
        <Container style={styles.container}>
        <Header hasTabs style={{backgroundColor: "#222325"}} androidStatusBarColor="#222325" iosBarStyle="light-content">
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon name="md-arrow-round-back" style={{color:"white"}}/>
            </Button>
          </Left>
          <Body>
            <Title style={{color:"white"}}>Sign Up</Title>
          </Body>
          <Right />
        </Header>

        <Content contentContainerStyle={styles.content}>
        <Image source={require("./../../../assets/screen.png")} style={styles.registerLogo}/>
          <Form style={styles.form}>
            <Item floatingLabel>
              <Label style={styles.formLabel}>Name</Label>
              <Input />
            </Item>
            <Item floatingLabel>
              <Label style={styles.formLabel}>Email</Label>
              <Input />
            </Item>
            <Item floatingLabel >
              <Label style={styles.formLabel}>Password</Label>
              <Input secureTextEntry />
            </Item>
            <Item floatingLabel secureTextEntry>
              <Label style={styles.formLabel}>Confirm Password</Label>
              <Input secureTextEntry />
            </Item>
          </Form>
          <Button
          block
          style={styles.registerButton}
          onPress={() => this.props.navigation.navigate(
            {
              key: "Login",
              routeName: "Login"
            }
            )}
            >
            <Text>Register</Text>
          </Button>
          <Text
          style={styles.loginText}
          onPress={() => this.props.navigation.navigate(
            {
              key: "Login",
              routeName: "Login"
            }
            )}
            >
            Already a user? Login here.
          </Text>
        </Content>
      </Container>
      );
    }
  }

export default Register;
