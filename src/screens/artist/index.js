import React, { Component } from 'react';
import {
  Container,
  Content,
  View,
  Button,
  Text,
  Card,
  Body,
  Title,
  Icon,
  Header,
  Left,
  Right,
  ActionSheet,
  Subtitle,
  Toast,
  Footer,
  FooterTab,
} from 'native-base';
import {
  Image,
  TouchableNativeFeedback,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  FlatList,
} from 'react-native';
import TouchableNativeFeed from '../../component/TouchableNativeFeedback';
import styles from './styles';
import MusicPlayerWrapper from '../musicPlayerWrapper/index';
console.disableYellowBox = true;
const topAlbums = [
  {
    name: 'New Music Friday UK',
    followers: '594089 followers',
    cover: require('./../../../assets/albumCover2.jpg'),
  },
  {
    name: 'The Pop List',
    followers: '556245 followers',
    cover: require('./../../../assets/albumCover1.jpg'),
  },
  {
    name: 'Summer Splash',
    followers: '464646 followers',
    cover: require('./../../../assets/albumCover4.jpg'),
  },
  {
    name: 'Pop Toppers',
    followers: '48446 followers',
    cover: require('./../../../assets/albumCover7.jpg'),
  },
  {
    name: 'Dazzling Dusty',
    followers: '23164 followers',
    cover: require('./../../../assets/albumCover3.jpg'),
  },
  {
    name: 'Indie Selects',
    followers: '656564 followers',
    cover: require('./../../../assets/albumCover6.jpg'),
  },
  {
    name: 'Campfire',
    followers: '8464 followers',
    cover: require('./../../../assets/albumCover5.jpg'),
  },
];

const topSongs = [
  {
    name: 'These Days (feat. Jess Glynne, Macklemore)',
    artists: 'Rudimental, Jess Glyme, Macklemore',
    img: require('./../../../assets/albumCover1.jpg'),
    url: 'https://www.sample-videos.com/audio/mp3/crowd-cheering.mp3',
  },
  {
    name: 'Shape of You',
    artists: 'Ed Sheeran',
    img: require('./../../../assets/albumCover2.jpg'),
    url:
      'https://assets-music.icons8.com/free-download/86/033d4211-a5e6-4006-835b-814c30239126.mp3?filename=rainbows.mp3',
  },
  {
    name: 'Closer',
    artists: 'The Chainsmokers',
    img: require('./../../../assets/albumCover3.jpg'),
    url:
      'https://assets-music.icons8.com/free-download/716/6b7b2ddf-ef3c-410e-bb74-eafe6cbdec26.mp3?filename=sunny.mp3',
  },
  {
    name: 'Magenta Riddim',
    artists: 'DJ Snake',
    img: require('./../../../assets/albumCover4.jpg'),
    url:
      'https://assets-music.icons8.com/free-download/919/5972dede-665d-4b82-8dc7-32d21a708aa8.mp3?filename=street-tables-cafe.mp3',
  },
  {
    name: 'Me Gente',
    artists: 'J Balvin, Willy William',
    img: require('./../../../assets/albumCover5.jpg'),
    url:
      'https://assets-music.icons8.com/free-download/694/bdc130ff-22fd-4ecf-95b3-440aa51b35fb.mp3?filename=spring-mood.mp3',
  },
  {
    name: "Somebody's Me",
    artists: 'Enrique Iglesias',
    img: require('./../../../assets/albumCover6.jpg'),
    url:
      'https://assets-music.icons8.com/free-download/617/aceb0efe-e441-4789-b778-c6ea1b0ad5b6.mp3?filename=funny.mp3',
  },
];

var ARTIST_BUTTONS = ['Follow Artist', 'Share', 'Shuffle Play', 'Cancel'];
var ARTIST_DESTRUCTIVE_INDEX = 3;
var ARTIST_CANCEL_INDEX = 3;

class Artist extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      following: false,
    };
  }

  follow = () => {
    this.setState({
      following: !this.state.following,
    });
    Toast.show({
      text:
        'You are now following ' +
        this.props.navigation.state.params.artistName +
        '!',
      duration: 2500,
      type: 'success',
    });
  };

  unfollow = () => {
    this.setState({
      following: !this.state.following,
    });
    Toast.show({
      text:
        'You unfollowed ' + this.props.navigation.state.params.artistName + '!',
      duration: 2500,
      type: 'success',
    });
  };

  render() {
    return (
      <Container style={styles.container}>
        <Header
          style={{ backgroundColor: 'red' }}
          androidStatusBarColor="#222325"
          iosBarStyle="light-content"
        >
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" style={styles.colorWhite} />
            </Button>
          </Left>
          <Body>
            <Title style={styles.headerText}>
              {this.props.navigation.state.params.artistName}
            </Title>
            <Subtitle style={styles.headerText}>Artist</Subtitle>
          </Body>
          <Right>
            <Button
              transparent
              onPress={() =>
                ActionSheet.show(
                  {
                    options: ARTIST_BUTTONS,
                    cancelButtonIndex: ARTIST_CANCEL_INDEX,
                    destructiveButtonIndex: ARTIST_DESTRUCTIVE_INDEX,
                    title: 'Artist Options',
                  },
                  buttonIndex => {
                    this.setState({ clicked: ARTIST_BUTTONS[buttonIndex] });
                  }
                )
              }
            >
              <Icon name="md-more" style={styles.colorWhite} />
            </Button>
          </Right>
        </Header>

        <Content>
          <View style={styles.center}>
            <Image
              source={this.props.navigation.state.params.artistImg}
              style={styles.artistImg}
            />
            <View style={styles.artistNameView}>
              <Text style={styles.artistName}>
                {this.props.navigation.state.params.artistName}
              </Text>
            </View>
            <View style={{ marginVertical: 5 }}>
              {!this.state.following ? (
                <Button
                  small
                  bordered
                  rounded
                  iconLeft
                  style={{ borderColor: 'black', backgroundColor: 'black' }}
                  onPress={this.follow}
                >
                  <Icon name="ios-add" style={{ color: 'black' }} />
                  <Text style={{ color: 'black', fontWeight: '200' }}>
                    Follow
                  </Text>
                </Button>
              ) : (
                <Button
                  small
                  bordered
                  rounded
                  iconLeft
                  style={{ borderColor: 'black', backgroundColor: 'black' }}
                  onPress={this.unfollow}
                >
                  <Icon name="ios-checkmark" style={{ color: 'black' }} />
                  <Text style={{ color: 'black', fontWeight: '200' }}>
                    Following
                  </Text>
                </Button>
              )}
            </View>
            <View style={{ marginVertical: 5 }}>
              <Button
                rounded
                large
                onPress={() =>
                  this.props.navigation.navigate({
                    key: 'Player',
                    routeName: 'Player',
                    params: {
                      tunes: topSongs,
                      currentTrackIndex: 0,
                      playlistName: this.props.navigation.state.params
                        .artistName,
                    },
                  })
                }
                style={styles.shuffleBtn}
              >
                <Text uppercase style={styles.shuffle}>
                  Start Radio
                </Text>
              </Button>
            </View>
          </View>

          <View style={styles.heading}>
            <Text style={styles.title}>Top Albums</Text>
            <Text style={styles.all}>View All</Text>
          </View>

          <View>
            <ScrollView
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={styles.scrollView}
            >
              <FlatList
                horizontal={true}
                data={topAlbums}
                keyExtractor={album => album.name}
                renderItem={album => (
                  <TouchableNativeFeed
                    onPress={() =>
                      this.props.navigation.navigate({
                        key: 'Album',
                        routeName: 'Album',
                        params: {
                          playlistImg: album.item.cover,
                          playlistName: album.item.name,
                        },
                      })
                    }
                    background={TouchableNativeFeedback.Ripple('black')}
                    delayPressIn={0}
                    useForeground
                  >
                    <Card style={styles.card} noShadow={true}>
                      <Image source={album.item.cover} style={styles.cardImg} />
                      <Body style={styles.cardItem}>
                        <View style={styles.radioCardName}>
                          <View style={{ flex: 0.5 }}>
                            <Text
                              style={styles.text}
                              lineBreakMode="tail"
                              ellipsizeMode="tail"
                              numberOfLines={1}
                            >
                              {album.item.name}
                            </Text>
                          </View>
                          <View style={{ flex: 0.5 }}>
                            <Text
                              uppercase
                              style={styles.cardSub}
                              lineBreakMode="tail"
                              ellipsizeMode="tail"
                              numberOfLines={1}
                            >
                              {album.item.followers}
                            </Text>
                          </View>
                        </View>
                      </Body>
                    </Card>
                  </TouchableNativeFeed>
                )}
              />
            </ScrollView>
          </View>

          <View style={styles.heading}>
            <Text style={styles.title}>Top Songs</Text>
            <Text style={styles.all}>View All</Text>
          </View>

          <View>
            <ScrollView
              horizontal
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={styles.scrollView}
            >
              <FlatList
                horizontal={true}
                data={topSongs}
                keyExtractor={song => song.name}
                renderItem={song => (
                  <TouchableNativeFeed
                    onPress={() =>
                      this.props.navigation.navigate({
                        key: 'Player',
                        routeName: 'Player',
                        params: {
                          tunes: topSongs,
                          currentTrackIndex: song.index,
                          playlistName: this.props.navigation.state.params
                            .artistName,
                        },
                      })
                    }
                    background={TouchableNativeFeedback.Ripple('white')}
                    delayPressIn={0}
                    useForeground
                  >
                    <Card style={styles.card} noShadow={true}>
                      <Image source={song.item.img} style={styles.cardImg} />
                      <Body style={styles.cardItem}>
                        <View style={styles.radioCardName}>
                          <View style={{ flex: 0.5 }}>
                            <Text
                              style={styles.text}
                              lineBreakMode="tail"
                              ellipsizeMode="tail"
                              numberOfLines={1}
                            >
                              {song.item.name}
                            </Text>
                          </View>
                          <View style={{ flex: 0.5 }}>
                            <Text
                              uppercase
                              style={styles.cardSub}
                              lineBreakMode="tail"
                              ellipsizeMode="tail"
                              numberOfLines={1}
                            >
                              {song.item.artists}
                            </Text>
                          </View>
                        </View>
                      </Body>
                    </Card>
                  </TouchableNativeFeed>
                )}
              />
            </ScrollView>
          </View>
        </Content>

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate({
              key: 'Player',
              routeName: 'Player',
              params: {
                tunes: [
                  {
                    name: 'These Days (feat. Jess Glynne, Macklemore)',
                    artists: 'Rudimental, Jess Glyme, Macklemore',
                    img: require('./../../../assets/albumCover1.jpg'),
                    url:
                      'https://assets-music.icons8.com/free-download/716/6b7b2ddf-ef3c-410e-bb74-eafe6cbdec26.mp3?filename=sunny.mp3',
                  },
                ],
                currentTrackIndex: 0,
                playlistName: 'Summer Splash',
              },
            })
          }
        >
          <Footer style={styles.footer}>
            <MusicPlayerWrapper minimal={true} />
          </Footer>
        </TouchableOpacity>
      </Container>
    );
  }
}

export default Artist;
