
import React, { Component } from "react";
import {
  Content,
  Text,
  Body,
  Left,
  Thumbnail,
  List,
  ListItem,
  Right,
  Button,
  Icon,
  ActionSheet,
  Container,
  Footer,
  FooterTab
} from "native-base";
import { View, Image, FlatList, TouchableOpacity } from "react-native";
import styles from "./styles";
import TouchableNativeFeed from "../../../component/TouchableNativeFeedback";
import MusicPlayerWrapper from "../../musicPlayerWrapper/index";
import Data from "../../../constants/constantData.js";
import yourAlbumsData from "./data.js";

const PLAYLIST_BUTTONS = yourAlbumsData.playListButton;
const PLAYLIST_DESTRUCTIVE_INDEX = yourAlbumsData.PLAYLIST_DESTRUCTIVE_INDEX;
const PLAYLIST_CANCEL_INDEX = yourAlbumsData.PLAYLIST_CANCEL_INDEX;
const datas = yourAlbumsData.data;
const player = Data.player;
const footerPlayer = Data.footerPlayer;

class YourAlbums extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <Content style={{ backgroundColor: "black" }}>
          <FlatList
            data={datas}
            keyExtractor={data => data.text}
            renderItem={data =>
              <ListItem
                thumbnail
                noBorder
                activeOpacity={1}
                androidRippleColor="white"
                onPress={() => this.props.navigation.navigate(
                  {
                    key: "Album",
                    routeName: "Album",
                    params: {
                      playlistImg: data.item.img,
                      playlistName: data.item.text
                    }
                  }
                )}
              >
                <View style={{ flexDirection: "row" }}>
                  <Left>
                    <Thumbnail square size={55} source={data.item.img} backgroundColor="grey" />
                  </Left>
                  <Body>
                    <Text style={styles.listText} lineBreakMode="tail" ellipsizeMode="tail" numberOfLines={1} >
                      {data.item.text}
                    </Text>
                    <Text lineBreakMode="tail" ellipsizeMode="tail" numberOfLines={1} note>
                      {data.item.note}
                    </Text>
                  </Body>
                  <Right>
                    <Button
                      transparent
                      onPress={() =>
                        ActionSheet.show(
                          {
                            options: PLAYLIST_BUTTONS,
                            cancelButtonIndex: PLAYLIST_CANCEL_INDEX,
                            destructiveButtonIndex: PLAYLIST_DESTRUCTIVE_INDEX,
                            title: "Album Options"
                          },
                          buttonIndex => {
                            this.setState({ clicked: PLAYLIST_BUTTONS[buttonIndex] });
                          }
                        )
                      }
                    >
                      <Icon name="md-more" style={{ color: "white" }} />
                    </Button>
                  </Right>
                </View>
                {/* </TouchableNativeFeed> */}
              </ListItem>
            }
          />
        </Content>
        <TouchableOpacity onPress={() => this.props.navigation.navigate(
          {
            key: player.key,
            routeName: player.routeName,
            params: {
              tunes: [{
                name: player.params.tunes[0].name,
                artists: player.params.tunes[0].artists,
                img: player.params.tunes[0].img,
                url: player.params.tunes[0].url
              }],
              currentTrackIndex: player.params.currentTrackIndex,
              playlistName: player.params.playlistName
            }
          }
        )}
        >
          <Footer style={styles.footer}>
            <MusicPlayerWrapper minimal={true} />
          </Footer>
        </TouchableOpacity>
      </Container>
    );
  }
}

export default YourAlbums;
