export default yourAlbumsData = 
{
    "data":[
        {
          "img": require("../../../../assets/albumCover1.jpg"),
          "text": "Fresh Hits",
          "note": "Ed Sheeran, Zayn Malik"
        },
        {
          "img": require("../../../../assets/albumCover2.jpg"),
          "text": "Wedding Cocktail",
          "note": "Arijit Singh, Neha Kakkar"
        },
        {
          "img": require("../../../../assets/albumCover3.jpg"),
          "text": "Unplugged",
          "note": "Sanam, Armaan Malik"
        },
        {
          "img": require("../../../../assets/albumCover4.jpg"),
          "text": "Weekend Vibes",
          "note": "Sonu Nigam, Atif Aslam"
        },
        {
          "img": require("../../../../assets/albumCover5.jpg"),
          "text": "Indipop Favorites",
          "note": "Neha Kakkar, Atif Aslam"
        },
        {
          "img": require("../../../../assets/albumCover6.jpg"),
          "text": "Sing Along",
          "note": "Ash King, Zubin Natiyal"
        },
        {
          "img": require("../../../../assets/albumCover4.jpg"),
          "text": "Feel Good Wednesdays",
          "note": "Taylor Swift, Enrique Iglesias"
        },
        {
          "img": require("../../../../assets/albumCover1.jpg"),
          "text": "Fam Jam",
          "note": "Justin Beiber, DJ Snake"
        }
      ],
      "playListButton":["Download Album", "Share", "Follow", "Shuffle Play", "Cancel"],
      "PLAYLIST_DESTRUCTIVE_INDEX" : 3,
      "PLAYLIST_CANCEL_INDEX":3
}