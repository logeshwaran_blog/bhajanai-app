
const React = require("react-native");
const { Dimensions } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;


export default {
  container: {
    backgroundColor: "#121212",
    flex: 1
  },
  searchHeader:{
     height: 60,
     backgroundColor: "black",
     paddingHorizontal: 10,
     paddingVertical: 8
  },
  searchInput:{
    textAlign: "center", 
    backgroundColor: "#222325", 
    color: "white", 
    height: 44, 
    maxHeight: 44,
    borderRadius: 5
  },
  center: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between"
  },
  text: {
    textAlign: "center",
    fontSize: 13,
    color: "white",
    fontWeight: "700",
  },
  listText: {
    color : "white",
    fontWeight: "bold"
  },
  searchIcon: {
    color: "white",
    fontSize: deviceHeight / 5,
    marginTop: deviceHeight / 4,
    marginBottom: deviceHeight / 20
  },
  serachBgText: {
    color: "white",
    paddingBottom: 10
  },
  serachFindText: {
    color: "white",
    width: deviceWidth / 1.5,
    textAlign: "center",
    fontSize: 12
  },
  footer: {
    backgroundColor: "#222325",
    height: 50
  }
};
