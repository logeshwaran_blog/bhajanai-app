
import React, { Component } from "react";
import {
  Content,
  Text,
  Body,
  Left,
  Thumbnail,
  List,
  ListItem,
  Right,
  Button,
  Icon,
  ActionSheet,
  Container,
  Footer,
  FooterTab,
} from "native-base";
import { View, Image, FlatList, TouchableOpacity } from "react-native";
import styles from "./styles";
import TouchableNativeFeed from "../../../component/TouchableNativeFeedback";
import MusicPlayerWrapper from "../../musicPlayerWrapper/index";
import Data from "../../../constants/constantData.js";
import yourPlaylistData from "./data.js";


const player = Data.player;
const footerPlayer = Data.footerPlayer;
const PLAYLIST_BUTTONS = yourPlaylistData.PLAYLIST_BUTTONS;
const PLAYLIST_DESTRUCTIVE_INDEX = yourPlaylistData.PLAYLIST_DESTRUCTIVE_INDEX;
const PLAYLIST_CANCEL_INDEX = yourPlaylistData.PLAYLIST_CANCEL_INDEX;
const datas = yourPlaylistData.data;

class YourPlaylist extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <Content style={styles.container}>
          <FlatList
            data={datas}
            keyExtractor={playlist => playlist.text}
            renderItem={(data) =>
              <ListItem
                thumbnail
                noBorder
                onPress={() => this.props.navigation.navigate(
                  {
                    key: "Album",
                    routeName: "Album",
                    params: {
                      playlistImg: data.item.img,
                      playlistName: data.item.text,
                    }
                  }
                )}
              >
                {/* <TouchableNativeFeed
                  onPress={() => this.props.navigation.navigate(
                    {
                      key: "Album",
                      routeName: "Album",
                      params: {
                        playlistImg: data.item.img,
                        playlistName: data.item.text,
                      }
                    }
                  )}
                > */}
                <View style={{ flexDirection: "row" }}>
                  <Left>
                    <Thumbnail square size={55} source={data.item.img} backgroundColor="grey" />
                  </Left>
                  <Body>
                    <Text style={styles.listText}>
                      {data.item.text}
                    </Text>
                    <Text numberOfLines={1} note>
                      {data.item.note}
                    </Text>
                  </Body>
                  <Right>
                    <Button
                      transparent
                      onPress={() =>
                        ActionSheet.show(
                          {
                            options: PLAYLIST_BUTTONS,
                            cancelButtonIndex: PLAYLIST_CANCEL_INDEX,
                            destructiveButtonIndex: PLAYLIST_DESTRUCTIVE_INDEX,
                            title: "Playlist Options"
                          },
                          buttonIndex => {
                            this.setState({ clicked: PLAYLIST_BUTTONS[buttonIndex] });
                          }
                        )
                      }
                    >
                      <Icon name="md-more" style={styles.icon} />
                    </Button>
                  </Right>
                </View>
                {/* </TouchableNativeFeed> */}
              </ListItem>
            }
          />
        </Content>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate(
            {
              key: player.key,
              routeName: player.routeName,
              params: {
                tunes: [{
                  name: player.params.tunes[0].name,
                  artists: player.params.tunes[0].artists,
                  img: player.params.tunes[0].img,
                  url: player.params.tunes[0].url
                }],
                currentTrackIndex: player.params.currentTrackIndex,
                playlistName: player.params.playlistName
              }
            }
          )}
        >
          <Footer style={styles.footer}>
            <MusicPlayerWrapper minimal={true} />
          </Footer>
        </TouchableOpacity>
      </Container>
    );
  }
}

export default YourPlaylist;
