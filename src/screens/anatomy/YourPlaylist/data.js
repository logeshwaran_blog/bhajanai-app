export default yourPlaylistData= 
{
    "data":[
        {
          "img": require("../../../../assets/albumCover1.jpg"),
          "text": "Chill Mornings",
          "note": "33 Songs"
        },
        {
          "img": require("../../../../assets/albumCover2.jpg"),
          "text": "Fusion",
          "note": "20 Songs"
        },
        {
          "img": require("../../../../assets/albumCover6.jpg"),
          "text": "EDM",
          "note": "13 Songs"
        },
        {
          "img": require("../../../../assets/albumCover5.jpg"),
          "text": "Rainy Mood",
          "note": "11 Songs"
        },
        {
          "img": require("../../../../assets/albumCover3.jpg"),
          "text": "Fav English",
          "note": "68 Songs"
        },
        {
          "img": require("../../../../assets/albumCover4.jpg"),
          "text": "Rockers",
          "note": "57 Songs"
        },
        {
          "img": require("../../../../assets/albumCover3.jpg"),
          "text": "Party Poppers",
          "note": "42 Songs"
        },
        {
          "img": require("../../../../assets/albumCover6.jpg"),
          "text": "Starred Songs",
          "note": "39 Songs"
        }
      ],
      "PLAYLIST_BUTTONS":["Download Playlist", "Share", "Follow", "Shuffle Play", "Cancel"],
      "PLAYLIST_DESTRUCTIVE_INDEX" : 3,
      "PLAYLIST_CANCEL_INDEX":3

      
}