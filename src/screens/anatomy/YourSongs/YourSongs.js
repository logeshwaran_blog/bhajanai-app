
import React, { Component } from "react";
import {
  Content,
  Text,
  Body,
  Left,
  Thumbnail,
  List,
  ListItem,
  Right,
  Button,
  Icon,
  ActionSheet,
  Footer,
  FooterTab,
  Container
} from "native-base";
import { View, Image, FlatList, TouchableOpacity } from "react-native";
import styles from "./styles";
import TouchableNativeFeed from "../../../component/TouchableNativeFeedback";
import MusicPlayerWrapper from "../../musicPlayerWrapper/index";
import Data from "../../../constants/constantData.js";
import yourSongsData from "./data.js";


const player = Data.player;
const footerPlayer = Data.footerPlayer;
const SONG_BUTTONS = yourSongsData.SONG_BUTTONS;
const SONG_DESTRUCTIVE_INDEX = yourSongsData.SONG_DESTRUCTIVE_INDEX;
const SONG_CANCEL_INDEX = yourSongsData.SONG_CANCEL_INDEX;
const songs = yourSongsData.data;


class YourSongs extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <Content style={styles.container}>
          <FlatList
            data={songs}
            keyExtractor={song => song.name}
            renderItem={(song) =>
              <ListItem
                thumbnail
                noBorder
                onPress={() => this.props.navigation.navigate(
                  {
                    key: player.key,
                    routeName: player.routeName,
                    params: {
                      tunes: songs,
                      currentTrackIndex: song.index,
                      playlistName: player.playlistName
                    }
                  }
                )}
              >
                {/* <TouchableNativeFeed
              onPress={() => this.props.navigation.navigate(
                {
                  key: player.key,
                  routeName: player.routeName,
                  params: {
                    tunes: songs,
                    currentTrackIndex: index,
                    playlistName: player.playlistName
                  }
                }
              )}
              > */}
                <View style={{ flexDirection: "row" }}>
                  <Left>
                    <Thumbnail square size={55} source={song.item.img} backgroundColor="grey" />
                  </Left>
                  <Body>
                    <Text style={styles.listText}>
                      {song.item.name}
                    </Text>
                    <Text numberOfLines={1} note>
                      {song.item.artists}
                    </Text>
                  </Body>
                  <Right>
                    <Button
                      transparent
                      onPress={() =>
                        ActionSheet.show(
                          {
                            options: SONG_BUTTONS,
                            cancelButtonIndex: SONG_CANCEL_INDEX,
                            destructiveButtonIndex: SONG_DESTRUCTIVE_INDEX,
                            title: "Song Options"
                          },
                          buttonIndex => {
                            this.setState({ clicked: SONG_BUTTONS[buttonIndex] });
                          }
                        )
                      }
                    >
                      <Icon name="md-more" style={styles.icon} />
                    </Button>
                  </Right>
                </View>
                {/* </TouchableNativeFeed> */}
              </ListItem>
            }
          />
        </Content>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate(
            {
              key: player.key,
              routeName: player.routeName,
              params: {
                tunes: [{
                  name: player.params.tunes[0].name,
                  artists: player.params.tunes[0].artists,
                  img: player.params.tunes[0].img,
                  url: player.params.tunes[0].url
                }],
                currentTrackIndex: player.params.currentTrackIndex,
                playlistName: player.params.playlistName
              }
            }
          )}
        >
          <Footer style={styles.footer}>
            <MusicPlayerWrapper minimal={true} />
          </Footer>
        </TouchableOpacity>
      </Container>
    );
  }
}

export default YourSongs;
