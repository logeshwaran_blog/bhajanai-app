
import YourPlaylist from "../YourPlaylist/YourPlaylist";
import YourSongs from "../YourSongs/YourSongs";
import YourAlbums from "../YourAlbums/YourAlbums";
import YourArtists from "../YourArtists/YourArtists";
import { createMaterialTopTabNavigator, createAppContainer } from "react-navigation";

const TabNavigator = createMaterialTopTabNavigator(
  {
    Playlist : {
      screen: YourPlaylist,
      navigationOptions: {
        tabBarLabel: "Playlist"
      }
    },
    Songs : {
      screen: YourSongs,
      navigationOptions: {
        tabBarLabel: "Songs"
      }
    },
    ALbums : {
      screen: YourAlbums,
      navigationOptions: {
        tabBarLabel: "Albums"
      }
    },
    Library : {
      screen: YourArtists,
      navigationOptions: {
        tabBarLabel: "Artists"
      }
    }
  },
  {
    swipeEnabled: true,
    lazy: false,
    navigatorStyle: {
      statusBarTextColorScheme: "light",
      statusBarColor: "#222325"
    },
    navigationOptions: ({navigation}) => ({
      headerTitleStyle: {
        color: "white"
      },
    }),
    tabBarOptions: {
      style: {
        backgroundColor: "#222325",
        color: "white"
      },
      labelStyle: {
        color: "white"
      },
      activeTintColor: "white",
      indicatorStyle: {
        backgroundColor: "white"
      }
    },
    tabBarPosition: "top",
    animationEnabled: true
  }
);

export default createAppContainer(TabNavigator);
