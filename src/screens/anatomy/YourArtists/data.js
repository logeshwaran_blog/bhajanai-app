export default YourArtistsData =
{
    "data":[
        {
          "img": require("../../../../assets/albumCover1.jpg"),
          "name": "Zayn Malik",
          "note": "106 Songs"
        },
        {
          "img": require("../../../../assets/albumCover2.jpg"),
          "name": "Arijit Singh",
          "note": "269 Songs"
        },
        {
          "img": require("../../../../assets/albumCover3.jpg"),
          "name": "Taylor Swift",
          "note": "52 Songs"
        },
        {
          "img": require("../../../../assets/albumCover4.jpg"),
          "name": "Ed Sheeran",
          "note": "94 Songs"
        },
        {
          "img": require("../../../../assets/albumCover5.jpg"),
          "name": "Sia",
          "note": "273 Songs"
        },
        {
          "img": require("../../../../assets/albumCover6.jpg"),
          "name": "Charlie Puth",
          "note": "20 Songs"
        },
        {
          "img": require("../../../../assets/albumCover7.jpg"),
          "name": "Eminem",
          "note": "61 Songs"
        },
        {
          "img": require("../../../../assets/browseCover.jpg"),
          "name": "Bruno Mars",
          "note": "84 Songs"
        }
      ],
      "ARTIST_BUTTONS" : ["Follow Artist", "Share", "Shuffle Play", "Cancel"],
      "ARTIST_DESTRUCTIVE_INDEX":3,
      "ARTIST_CANCEL_INDEX":3
}