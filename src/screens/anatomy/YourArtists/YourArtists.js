
import React, { Component } from "react";
import {
  Content,
  Text,
  Body,
  Left,
  Thumbnail,
  List,
  ListItem,
  Right,
  Button,
  Icon,
  ActionSheet,
  Footer,
  FooterTab,
  Container
} from "native-base";
import { View, Image, FlatList, TouchableOpacity } from "react-native";
import styles from "./styles";
import TouchableNativeFeed from "../../../component/TouchableNativeFeedback";
import MusicPlayerWrapper from "../../musicPlayerWrapper/index";
import Data from "../../../constants/constantData.js";
import YourArtistsData from "./data.js";

const ARTIST_BUTTONS = YourArtistsData.ARTIST_BUTTONS;
const ARTIST_DESTRUCTIVE_INDEX = YourArtistsData.ARTIST_DESTRUCTIVE_INDEX;
const ARTIST_CANCEL_INDEX = YourArtistsData.ARTIST_CANCEL_INDEX;
const player = Data.player;
const footerPlayer = Data.footerPlayer;
const artists = YourArtistsData.data;


class YourArtists extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Container>
        <Content style={styles.container}>
          <FlatList
            data={artists}
            keyExtractor={artist => artist.name}
            renderItem={artist =>
              <ListItem
                thumbnail
                noBorder
                onPress={() => this.props.navigation.navigate(
                  {
                    key: "Artist",
                    routeName: "Artist",
                    params: {
                      artistImg: artist.item.img,
                      artistName: artist.item.name
                    }
                  }
                )}
              >
                {/* <TouchableNativeFeed
              onPress={() => this.props.navigation.navigate(
                {
                  key: "Artist",
                  routeName: "Artist",
                  params: {
                    artistImg: artist.item.img,
                    artistName: artist.item.name
                  }
                }
              )}
              > */}
                <View style={{ flexDirection: "row" }}>
                  <Left>
                    <Thumbnail square size={55} source={artist.item.img} backgroundColor="grey" />
                  </Left>
                  <Body>
                    <Text style={styles.listText}>
                      {artist.item.name}
                    </Text>
                    <Text numberOfLines={1} note>
                      {artist.item.note}
                    </Text>
                  </Body>
                  <Right>
                    <Button
                      transparent
                      onPress={() =>
                        ActionSheet.show(
                          {
                            options: ARTIST_BUTTONS,
                            cancelButtonIndex: ARTIST_CANCEL_INDEX,
                            destructiveButtonIndex: ARTIST_DESTRUCTIVE_INDEX,
                            title: "Artist Options"
                          },
                          buttonIndex => {
                            this.setState({ clicked: ARTIST_BUTTONS[buttonIndex] });
                          }
                        )
                      }
                    >
                      <Icon name="md-more" style={styles.icon} />
                    </Button>
                  </Right>
                </View>
                {/* </TouchableNativeFeed> */}
              </ListItem>}
          />
        </Content>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate(
            {
              key: player.key,
              routeName: player.routeName,
              params: {
                tunes: [{
                  name: player.params.tunes[0].name,
                  artists: player.params.tunes[0].artists,
                  img: player.params.tunes[0].img,
                  url: player.params.tunes[0].url
                }],
                currentTrackIndex: player.params.currentTrackIndex,
                playlistName: player.params.playlistName
              }
            }
          )}
        >
          <Footer style={styles.footer}>
            <MusicPlayerWrapper minimal={true} />
          </Footer>
        </TouchableOpacity>
      </Container>
    );
  }
}

export default YourArtists;
