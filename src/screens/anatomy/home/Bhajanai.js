/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  Container,
  Content,
  Card,
  List,
  Body,
  Footer,
  FooterTab,
} from 'native-base';
import {
  ScrollView,
  View,
  Image,
  TouchableNativeFeedback,
  FlatList,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import TouchableNativeFeed from '../../../component/TouchableNativeFeedback';
import styles from './styles';
import MusicPlayerWrapper from '../../musicPlayerWrapper/index';

import albumData from './albums.js';
import Data from '../../../constants/constantData.js';

console.disableYellowBox = true;

import ImageCarousel from '../../Util/ImageCarousel';

// function copy(o) {
//   var output, v, key;
//   output = Array.isArray(o) ? [] : {};
//   for (key in o) {
//     v = o[key];
//     output[key] = typeof v === 'object' ? copy(v) : v;
//   }
//   return output;
// }

// const shuffle = array => {
//   let currentIndex = array.length,
//     temp,
//     randomIndex;
//   while (currentIndex !== 0) {
//     randomIndex = Math.floor(Math.random() * currentIndex);
//     currentIndex -= 1;
//     temp = array[currentIndex];
//     array[currentIndex] = array[randomIndex];
//     array[randomIndex] = temp;
//   }
//   let arr = copy(array);
//   return arr;
// };

const albums = albumData.albums;
const player = Data.player;
const footerPlayer = Data.footerPlayer;

const images = [
  'http://beta.bhajanai.com/wp-content/uploads/2019/11/god-murugan-latest-hd-photos-wallpapers-1080p-xxh.jpg',
  'http://beta.bhajanai.com/wp-content/uploads/2019/11/god-murugan-latest-hd-photos-wallpapers-1080p-afm.jpg',
];

class Bhajanai extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <Container style={styles.container}>
        <Content>
          <View style={{alignItems: 'center', justifyContent: 'center'}}>
            <ImageCarousel images={images} />
          </View>
          <View>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={styles.title}>Bhajanai Albums</Text>
            </View>
            <View>
              <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={styles.scrollView}>
                <FlatList
                  horizontal={true}
                  data={albums}
                  removeClippedSubviews={false}
                  keyExtractor={album => album.name}
                  renderItem={album => (
                    <TouchableNativeFeed
                      onPress={() =>
                        this.props.navigation.navigate({
                          key: 'Album',
                          routeName: 'Album',
                          params: {
                            playlistImg: album.item.cover,
                            playlistName: album.item.name,
                          },
                        })
                      }
                      // onPress={() => navigation.navigate('Songs')}
                      background={TouchableNativeFeedback.Ripple('white')}
                      delayPressIn={0}
                      useForeground>
                      <Card style={styles.card} noShadow={true}>
                        <ImageBackground
                          source={album.item.cover}
                          style={styles.cardImg}
                          imageStyle={{borderRadius: 6}}>
                          <Text
                            style={{
                              color: 'white',
                              left: 16,
                              top: 120,
                              position: 'absolute',
                            }}>
                            {album.item.name}
                          </Text>
                        </ImageBackground>
                      </Card>
                    </TouchableNativeFeed>
                  )}
                />
              </ScrollView>
            </View>
          </View>
        </Content>

        <TouchableOpacity
        // onPress={() =>
        //   this.props.navigation.navigate({
        //     key: player.key,
        //     routeName: player.routeName,
        //     params: {
        //       tunes: [
        //         {
        //           name: player.params.tunes[0].name,
        //           artists: player.params.tunes[0].artists,
        //           img: player.params.tunes[0].img,
        //           url: player.params.tunes[0].url,
        //         },
        //       ],
        //       currentTrackIndex: player.params.currentTrackIndex,
        //       playlistName: player.params.playlistName,
        //     },
        //   })
        // }
        >
          <Footer style={styles.footer}>
            <MusicPlayerWrapper minimal={true} />
          </Footer>
        </TouchableOpacity>
      </Container>
    );
  }
}
export default Bhajanai;
