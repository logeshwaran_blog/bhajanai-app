/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';

const {height, width} = Dimensions.get('window');

class MoreScreen extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <View style={{flex: 1}}>
          <View
            style={{
              width: width,
              height: 40,
              backgroundColor: '#fff',
              flexDirection: 'row',
              marginTop: 7,
            }}>
            <View style={{width: width - 70}}>
              <Text style={styles.titleH4}>Songs</Text>
            </View>
          </View>
          <View
            style={{
              width: width,
              height: 60,
              backgroundColor: '#fff',
              flexDirection: 'row',
              marginTop: 7,
            }}>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => navigation.navigate('Artistlist')}>
              <View
                style={{
                  width: width,
                  height: 60,
                  flexDirection: 'row',
                }}>
                <View style={{width: width - 70}}>
                  <Text style={styles.titleH2}>Artist</Text>
                </View>
                <View
                  style={{
                    width: 38,
                    height: 22,
                    marginLeft: 10,
                    marginTop: 18,
                  }}>
                  <Image
                    style={{height: 22, width: 22}}
                    source={require('./../../../../assets/icons/nav-icons/right.png')}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: width,
              height: 60,
              backgroundColor: '#fff',
              flexDirection: 'row',
              marginTop: 7,
            }}>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => navigation.navigate('Godnamelist')}>
              <View
                style={{
                  width: width,
                  height: 60,

                  flexDirection: 'row',
                }}>
                <View style={{width: width - 70}}>
                  <Text style={styles.titleH2}>God</Text>
                </View>
                <View
                  style={{
                    width: 38,
                    height: 22,
                    marginLeft: 10,
                    marginTop: 18,
                  }}>
                  <Image
                    style={{height: 22, width: 22}}
                    source={require('./../../../../assets/icons/nav-icons/right.png')}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: width,
              height: 40,
              backgroundColor: '#fff',
              flexDirection: 'row',
              marginTop: 7,
            }}>
            <View style={{width: width - 70}}>
              <Text style={styles.titleH4}>Other</Text>
            </View>
          </View>

          <View
            style={{
              width: width,
              height: 60,
              backgroundColor: '#fff',
              flexDirection: 'row',
              marginTop: 7,
            }}>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => navigation.navigate('Photos')}>
              <View
                style={{
                  width: width,
                  height: 60,

                  flexDirection: 'row',
                }}>
                <View style={{width: width - 70}}>
                  <Text style={styles.titleH2}>Photos</Text>
                </View>
                <View
                  style={{
                    width: 38,
                    height: 22,
                    marginLeft: 10,
                    marginTop: 18,
                  }}>
                  <Image
                    style={{height: 22, width: 22}}
                    source={require('./../../../../assets/icons/nav-icons/right.png')}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: width,
              height: 60,
              backgroundColor: '#fff',
              flexDirection: 'row',
              marginTop: 7,
            }}>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => navigation.navigate('Videos')}>
              <View
                style={{
                  width: width,
                  height: 60,

                  flexDirection: 'row',
                }}>
                <View style={{width: width - 70}}>
                  <Text style={styles.titleH2}>Videos</Text>
                </View>
                <View
                  style={{
                    width: 38,
                    height: 22,
                    marginLeft: 10,
                    marginTop: 18,
                  }}>
                  <Image
                    style={{height: 22, width: 22}}
                    source={require('./../../../../assets/icons/nav-icons/right.png')}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: width,
              height: 60,
              backgroundColor: '#fff',
              flexDirection: 'row',
              marginTop: 7,
            }}>
            <TouchableOpacity
              activeOpacity={0.9}
              onPress={() => navigation.navigate('About')}>
              <View
                style={{
                  width: width,
                  height: 60,

                  flexDirection: 'row',
                }}>
                <View style={{width: width - 70}}>
                  <Text style={styles.titleH2}>About</Text>
                </View>
                <View
                  style={{
                    width: 38,
                    height: 22,
                    marginLeft: 10,
                    marginTop: 18,
                  }}>
                  <Image
                    style={{height: 22, width: 22}}
                    source={require('./../../../../assets/icons/nav-icons/right.png')}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: width,
              height: 60,
              backgroundColor: '#fff',
              flexDirection: 'row',
              marginTop: 7,
            }}>
            <View style={{width: width - 70}}>
              <Text style={styles.titleH3}>Version 1.0.0</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default MoreScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f7f7f7',
    width: width,
    height: height,
    justifyContent: 'space-around',
  },
  listBg: {
    flex: 1,
    backgroundColor: '#3c3c3c',
    alignItems: 'center',
    justifyContent: 'center',
    height: 60,
    width: 320,
  },
  title: {
    textAlign: 'center',
    fontSize: 40,
    fontWeight: '700',
    lineHeight: 40,
    marginTop: 20,
    color: '#333333',
  },
  titleH2: {
    fontSize: 18,
    lineHeight: 24,
    color: '#3c3c3c',
    marginLeft: 26,
    marginTop: 18,
  },
  titleH3: {
    fontSize: 14,
    lineHeight: 24,
    color: '#3c3c3c',
    marginLeft: 26,
    marginTop: 18,
  },
  titleH4: {
    fontSize: 16,
    fontWeight: '700',
    lineHeight: 24,
    color: '#3c3c3c',
    marginLeft: 26,
    marginTop: 8,
  },
  infop1: {
    fontSize: 12,
    lineHeight: 24,
    color: '#3c3c3c',
    marginLeft: 12,
    marginTop: 4,
  },
});
