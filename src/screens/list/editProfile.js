/**
 * Audrix (https://www.enappd.com/audrix)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import React from "react";
import {
  Button,
  Form,
  Item,
  Label,
  Input,
  Text,
} from "native-base";
import { View } from "react-native";
import styles from "./styles"

export default () =>
  <View>
    <Form>
      <Item stackedLabel>
        <Label style={styles.padTop}>Name</Label>
        <Input><Text>Enappd</Text></Input>
      </Item>
      <Item stackedLabel>
        <Label style={styles.padTop}>Mobile Number</Label>
        <Input keyboardType="numeric"><Text>987654321</Text></Input>
      </Item>
      <Item stackedLabel last>
        <Label style={styles.padTop}>Email</Label>
        <Input keyboardType="email-address"><Text>admin@enappd.com</Text></Input>
      </Item>
    </Form>
    <Button block style={styles.SaveBtn}>
      <Text>Save Changes</Text>
    </Button>
  </View>
  ;

