/**
 * Audrix (https://www.enappd.com/audrix)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import React, { Component } from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Text,
  Left,
  Right,
  Body,
  Separator,
  ListItem,
  Picker,
  Footer,
  FooterTab
} from "native-base";
import { Switch, Modal, View, Image } from "react-native";
import styles from "./styles";
import ChangePassword from "./changePassword";
import EditProfile from "./editProfile";

class Settings extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      quality: undefined,
      lang: undefined,
      eq: false,
      mob: false,
      email: false,
      changePasswordModal: false,
      editProfileModal: false
    };
  }

  onQualityChange(value) {
    this.setState({
      quality: value,
    });
  }

  onLangChange(value) {
    this.setState({
      lang: value
    });
  }

  toggleEq = () => {
    this.setState({
      eq: !this.state.eq
    });
  }

  toggleMob = () => {
    this.setState({
      mob: !this.state.mob
    });
  }

  toggleEmail = () => {
    this.setState({
      email: !this.state.email
    });
  }

  setChangePasswordModal = () => {
    this.setState({
      changePasswordModal: true
    });
  }
  closeChangePasswordModal = () => {
    this.setState({
      changePasswordModal: !this.state.changePasswordModal
    });
  }

  setEditProfileModal = () => {
    this.setState({
      editProfileModal: true
    });
  }
  closeEditProfileModal = () => {
    this.setState({
      editProfileModal: !this.state.editProfileModal
    });
  }
  render() {
    return (
      <Container style={styles.container}>
        <Header style={styles.header} androidStatusBarColor="#222325" iosBarStyle="light-content">
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="menu" style={styles.colorWhite} />
            </Button>
          </Left>
          <Body>
            <Title style={styles.colorWhite}>Settings</Title>
          </Body>
          <Right />
        </Header>

        <Content>
          <Separator>
            <Text style={styles.separator}>Music & Playback</Text>
          </Separator>
          <ListItem>
            <Left>
              <Text>Streaming Quality</Text>
            </Left>
            <Right>
              <Picker
                iosHeader="Select quality"
                iosIcon={<Icon name="arrow-down" style={styles.colorBlue} />}
                mode="dropdown"
                placeholder="Select Quality"
                placeholderStyle={{ color: "grey" }}
                placeholderIconColor="black"
                selectedValue={this.state.quality}
                onValueChange={this.onQualityChange.bind(this)}
                style={styles.listItemRightPicker}
              >
                <Picker.Item label="Auto" value="auto" />
                <Picker.Item label="High" value="high" />
                <Picker.Item label="Medium" value="med" />
                <Picker.Item label="Low" value="low" />
              </Picker>
            </Right>
          </ListItem>
          <ListItem>
            <Left >
              <Text>Music Languauge</Text>
            </Left>
            <Right >
              <Picker
                iosHeader="Select Language"
                iosIcon={<Icon name="arrow-down" style={styles.colorBlue} />}
                mode="dropdown"
                placeholder="Select Language"
                placeholderStyle={{ color: "grey" }}
                placeholderIconColor="black"
                selectedValue={this.state.lang}
                onValueChange={this.onLangChange.bind(this)}
                style={styles.listItemRightPicker}
              >
                <Picker.Item label="English" value="english" />
                <Picker.Item label="Hindi" value="hindi" />
                <Picker.Item label="Punjabi" value="punjabi" />
              </Picker>
            </Right>
          </ListItem>
          <ListItem last onPress={this.toggleEq}>
            <Left>
              <Text>Equalizer</Text>
            </Left>
            <Right>
              <Switch
                value={this.state.eq}
                onValueChange={this.toggleEq}
                trackColor="black"
                ios_backgroundColor="red"
                thumbColor="#666666"
              />
            </Right>
          </ListItem>

          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.editProfileModal}
            onRequestClose={() => {
              this.props.navigation.goBack();
            }}>
            <Container style={styles.container}>
              <Header style={{ backgroundColor: "#222325" }} androidStatusBarColor="#222325" iosBarStyle="light-content">
                <Left>
                  <Button transparent large onPress={this.closeEditProfileModal}>
                    <Icon name="md-close" style={styles.white} />
                  </Button>
                </Left>
                <Body style={styles.headerTitle}>
                  <Title style={styles.headerTitle}>Forgot Password</Title>
                </Body>
                <Right />
              </Header>
              <Content>
                <EditProfile />
              </Content>
            </Container>
          </Modal>

          <Separator>
            <Text style={styles.separator}>Social</Text>
          </Separator>
          <ListItem onPress={this.setEditProfileModal}>
            <Text>Edit Profile</Text>
          </ListItem>
          <ListItem onPress={this.toggleMob}>
            <Left>
              <Text>Mobile Notifications</Text>
            </Left>
            <Switch
              value={this.state.mob}
              onValueChange={this.toggleMob}
              trackColor="black"
              ios_backgroundColor="red"
              thumbColor="#666666"
            />
          </ListItem>
          <ListItem last onPress={this.toggleEmail}>
            <Left>
              <Text>Email Notifications</Text>
            </Left>
            <Switch
              value={this.state.email}
              onValueChange={this.toggleEmail}
              trackColor="black"
              ios_backgroundColor="red"
              thumbColor="#666666"
            />
          </ListItem>

          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.changePasswordModal}
            onRequestClose={() => {
              this.props.navigation.goBack();
            }}>
            <Container style={styles.container}>
              <Header style={{ backgroundColor: "#222325" }} androidStatusBarColor="#222325" iosBarStyle="light-content">
                <Left>
                  <Button transparent large onPress={this.closeChangePasswordModal}>
                    <Icon name="md-close" style={styles.white} />
                  </Button>
                </Left>
                <Body style={styles.headerTitle}>
                  <Title style={styles.headerTitle}>Forgot Password</Title>
                </Body>
                <Right />
              </Header>
              <Content>
                <ChangePassword />
              </Content>
            </Container>
          </Modal>

          <Separator>
            <Text style={styles.separator}>Account Settings</Text>
          </Separator>
          <ListItem last onPress={this.setChangePasswordModal}>
            <Text>Change Password</Text>
          </ListItem>
          {/* <ListItem last
          onPress={() => this.props.navigation.navigate("Login")}>
            <Text>Logout</Text>
          </ListItem> */}
        </Content>

      </Container >
    );
  }
}

export default Settings;
