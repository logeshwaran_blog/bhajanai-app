/**
 * Audrix (https://www.enappd.com/audrix)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import React from "react";
import {
  Button,
  Form,
  Item,
  Label,
  Input,
  Text,
} from "native-base";
import { View } from "react-native";
import styles from "./styles";


export default () =>
  <View>
    <Form>
      <Item floatingLabel>
        <Label style={styles.padTop}>Current Password</Label>
        <Input secureTextEntry />
      </Item>
      <Item floatingLabel>
        <Label style={styles.padTop}>New Password</Label>
        <Input secureTextEntry />
      </Item>
      <Item floatingLabel last>
        <Label style={styles.padTop}>Confirm Password</Label>
        <Input secureTextEntry />
      </Item>
    </Form>
    <Button block style={styles.SaveBtn}>
      <Text>Change Password</Text>
    </Button>
  </View>
  ;

