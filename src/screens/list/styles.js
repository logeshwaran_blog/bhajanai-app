/**
 * Audrix (https://www.enappd.com/audrix)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

const React = require("react-native");
const { Dimensions } = React;
const deviceWidth = Dimensions.get("window").width;

export default {
  container: {
    backgroundColor: "#FFF"
  },
  header: {
    backgroundColor: "#222325"
  },

  colorWhite: {
    color: "white"
  },
  colorBlack: {
    color: "black"
  },
  text: {
    alignSelf: "center",
    marginBottom: 7
  },
  mb: {
    marginBottom: 15
  },
  separator: {
    fontSize: 13
  },
  padTop: {
    paddingTop: 4
  },
  SaveBtn: {
    margin: 15,
    marginTop: 50,
    backgroundColor: "black"
  },
  listItemRightPicker: {
    width: deviceWidth/2
  },
  colorBlue :{
    color: "blue"
  },
  footer: {
    backgroundColor: "#222325",
    height: 50
  },
  white:{
    color: 'white'
  },
  headerTitle:{
    color: 'white',
    width: deviceWidth / 1.5
  }
};
