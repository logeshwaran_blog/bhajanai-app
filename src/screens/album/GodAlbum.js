/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Container,
  Content,
  View,
  Button,
  Text,
  ListItem,
  Body,
  Title,
  Icon,
  Header,
  Left,
  Right,
  Footer,
} from 'native-base';
import {
  Image,
  TouchableOpacity,
  FlatList,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import TouchableNativeFeed from '../../component/TouchableNativeFeedback';
import styles from './styles';
import MusicPlayerWrapper from '../musicPlayerWrapper/index';
// import albumData from './data.js';
// import Data from '../../constants/constantData.js';

// const player = Data.player;
// const footerPlayer = Data.footerPlayer;
// const songs = albumData.data;
// const PLAYLIST_BUTTONS = albumData.PLAYLIST_BUTTONS;
// const PLAYLIST_DESTRUCTIVE_INDEX = albumData.PLAYLIST_DESTRUCTIVE_INDEX;
// const PLAYLIST_CANCEL_INDEX = albumData.PLAYLIST_CANCEL_INDEX;
// const SONG_BUTTONS = albumData.SONG_BUTTONS;
// const SONG_DESTRUCTIVE_INDEX = albumData.SONG_DESTRUCTIVE_INDEX;
// const SONG_CANCEL_INDEX = albumData.SONG_CANCEL_INDEX;
const {width} = Dimensions.get('window');

class GodAlbum extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {isLoading: true, dataSource: []};
  }

  componentDidMount() {
    const itemId = this.props.navigation.getParam('itemId', 'NO-ID');
    console.log(itemId);
    console.log('itemId');
    return fetch('http://yb8.3a6.myftpupload.com/wp-json/api/v1/god/' + itemId)
      .then(response => response.json())
      .then(responseJson => {
        // console.log('responseJson');
        // console.log(responseJson);
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson.data.tracks,
          },
          function() {},
        );
      })
      .catch(error => {
        console.error(error);
      });
  }

  ListEmpty = () => {
    return (
      //View to show when list is empty
      <View style={styles.MainContainer}>
        <Text style={{textAlign: 'center'}}>No Data Found</Text>
      </View>
    );
  };
  renderItem = ({item, index}) => {
    // console.log('--- item ---');
    // console.log(item);

    return (
      <ListItem
        noBorder
        style={{
          width: width - 20,
          height: 50,
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          margin: 6,
        }}>
        <TouchableNativeFeed
          onPress={() => {
            this.props.navigation.navigate({
              key: 'Player',
              routeName: 'Player',
              params: {
                dataSource: this.state.dataSource,
                currentTrackIndex: index,
              },
            });
          }}>
          <View
            style={{
              width: 300,
              height: 50,
              flex: 1,
              flexDirection: 'row',
              borderRadius: 6,
              backgroundColor: '#f7f7f7',
            }}>
            <View style={{width: width - 90}}>
              <Text style={styles.titleH3}>{item.title}</Text>
            </View>
            <View
              style={{
                width: 24,
                height: 24,
                marginLeft: 10,
                marginTop: 13,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Image
                style={{height: 20, width: 20}}
                source={require('../../../assets/icons/music/play-button.png')}
              />
            </View>
          </View>
        </TouchableNativeFeed>
      </ListItem>
    );
  };

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator color="#455FE9" size="large" />
        </View>
      );
    } else {
      return (
        <Container style={styles.container}>
          <Header
            style={styles.header}
            androidStatusBarColor="#222325"
            iosBarStyle="light-content">
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}>
                <Icon name="ios-arrow-back" style={{color: 'black'}} />
              </Button>
            </Left>
            <Body>
              <Title style={styles.playListTitle}>
                {this.props.navigation.state.params.playlistName}
              </Title>
            </Body>
            <Right></Right>
          </Header>

          <Content>
            <View>
              <FlatList
                data={this.state.dataSource}
                keyExtractor={(x, i) => i}
                removeClippedSubviews={false}
                renderItem={this.renderItem}
                style={{backgroundColor: 'white'}}
                ListEmptyComponent={this.ListEmpty}
                //Message to show for the Empty list
              />
            </View>
          </Content>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate({
                key: 'Player',
                routeName: 'Player',
                params: {
                  dataSource: this.state.dataSource,
                  currentTrackIndex: 0,
                },
              });
            }}>
            <Footer style={styles.footer}>
              <MusicPlayerWrapper minimal={true} />
            </Footer>
          </TouchableOpacity>
        </Container>
      );
    }
  }
}

export default GodAlbum;
