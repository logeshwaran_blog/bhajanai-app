/**
 * Audrix (https://www.enappd.com/audrix)
 *
 * Copyright © 2018-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */

import React, { Component } from "react";
import { Image, FlatList } from "react-native";
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
} from "native-base";
import styles from "./style";
import TouchableNativeFeed from "./../../component/TouchableNativeFeedback";

const drawerCover = require("../../../assets/drawer-cover.png");
const datas = [
  {
    name: "Home",
    route: "TabHome",
    icon: "home",
    bg: "#C5F442"
  },
  {
    name: "Offline Content",
    route: "offlineContent",
    icon: "cloud-download",
    bg: "#fff"
  },
  {
    name: "Settings",
    route: "Settings",
    icon: "settings",
    bg: "#5DCEE2",
  },
  {
    name: "Login",
    route: "Login",
    icon: "login",
    bg: "#EFB406",
  },
];

class SideBar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4
    };
  }

  render() {
    return (
      <Container style={{ backgroundColor: "black" }}>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: "black", top: -1, }}
        >
          <Image source={drawerCover} style={styles.drawerCover} />
          <FlatList
            data={datas}
            keyExtractor={data => data.name}
            renderItem={data =>
              <ListItem
                noBorder
                androidRippleColor="transparent"
              >
                <TouchableNativeFeed
                  onPress={() => this.props.navigation.navigate(
                    {
                      key: data.item.route,
                      routeName: data.item.route
                    }
                  )}
                >
                  <Left>
                    <Icon
                      active
                      name={data.item.icon}
                      style={styles.icon}
                      type="MaterialCommunityIcons"
                    />
                    <Text style={styles.text}>
                      {data.item.name}
                    </Text>
                  </Left>
                </TouchableNativeFeed>
              </ListItem>
            }
          />
        </Content>
      </Container>
    );
  }
}

export default SideBar;
